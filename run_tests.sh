#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

"${DIR}/tests/xss3.sh"
"${DIR}/tests/flawed_json.sh"
