#!/usr/bin/env bash

TARGET_HOST=${TARGET_HOST:-localhost}
TARGET_PORT=${TARGET_PORT:-5000}
TARGET_URL=${TARGET_URL:-"http://${TARGET_HOST}:${TARGET_PORT}"}

cmd=(
    curl
        #--proxy "$HTTP_PROXY"
        -X POST
        -i
        -H "Content-Type: application/json"
        --data '{"data": "value"}'
        "${TARGET_URL}/api/items"
)
echo "==============================="
echo "FLAWED JSON TEST"
echo "==============================="

echo "${cmd[@]}"
"${cmd[@]}"
