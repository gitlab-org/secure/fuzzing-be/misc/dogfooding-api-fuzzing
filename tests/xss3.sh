#!/usr/bin/env bash

TARGET_HOST=${TARGET_HOST:-localhost}
TARGET_PORT=${TARGET_PORT:-5000}
TARGET_URL=${TARGET_URL:-"http://${TARGET_HOST}:${TARGET_PORT}"}

echo "==============================="
echo "XSS3 TEST"
echo "==============================="
echo "proxies:"
env | grep -i "_proxy"
echo
echo "==============================="

echo curl "${TARGET_URL}/xss3?a=k&name=Name"
curl --proxy "$HTTP_PROXY" "${TARGET_URL}/xss3?a=k&name=Name"
