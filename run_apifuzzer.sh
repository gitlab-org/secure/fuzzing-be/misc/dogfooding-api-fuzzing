#! /bin/bash

set -x

echo '======================'
echo "PEACH_ env vars:"
env | grep PEACH_

echo '======================'
echo "FUZZAPI_ env vars:"
env | grep FUZZAPI_

# Arguments:
# --noverify - Don't perform tls cert validation
# -v - Display pytest output
# --junit - Generate junit xml report
# -D - tag run with branch name

python /usr/local/bin/peachci --noverify -v --junit apifuzzer_test.xml -D $CI_COMMIT_BRANCH

# Exit codes: 0 - no faults, 1 - faults, 255 - testing failed
# Treat 0 and 1 as success
if [ $? -eq 1 ]
then
    exit 0
fi

# end
